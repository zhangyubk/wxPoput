Page({
  data: {
    isShowModel: false,//控制弹窗是否显示，默认不显示
    isShowConfirm: false,//是否只显示确定按钮，默认不是
    ModelId: 0,//弹窗id
    ModelTitle: '',//弹窗标题
    ModelContent: '',//弹窗文字内容
  },
  //阻断模态弹窗点击穿透
  preventTouchMove: function(){},
  //点击按钮的事件
  btnclick1: function(){
    this.showModel({
      ModelId: 0,
      ModelTitle: '标题(1)',
      ModelContent: '感谢使用wxPoput自定义模态弹窗,一个可以随意自定义样式的微信小程序弹窗插件。已经开源到GitHub上。'
    })
  },
  //点击按钮的事件
  btnclick2: function () {
    this.showModel({
      ModelId: 1,
      ModelTitle: '标题(2)',
      ModelContent: '感谢使用wxPoput自定义模态弹窗,一个可以随意自定义样式的微信小程序弹窗插件。已经开源到GitHub上。'
    })
  },
  //调用模态弹窗
  showModel: function (e){
    //将传过来的标题和内容展示到弹窗上
    this.setData({
      isShowModel: true,
      ModelId: e.ModelId,
      ModelTitle: e.ModelTitle,
      ModelContent: e.ModelContent
    })
  },
  //取消事件
  cancel: function(e){
    if (e.currentTarget.dataset.modelid == 0){
      console.log("用户点击了取消(1)")
    } else if (e.currentTarget.dataset.modelid == 1){
      console.log("用户点击了取消(2)")
    }
    //关闭模态弹窗
    this.setData({
      isShowModel: false
    })
  },
  //确定事件
  confirm: function(e){
    if (e.currentTarget.dataset.modelid == 0) {
      console.log("用户点击了确定(1)")
    } else if (e.currentTarget.dataset.modelid == 1) {
      console.log("用户点击了确定(2)")
    }
  }
})
