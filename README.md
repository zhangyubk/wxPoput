![wxPopup.png](http://upload-images.jianshu.io/upload_images/2312450-7f77ff2f38610113.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/800)

# wxPoput 
### 好用的微信小程序自定义模态弹窗插件 


最近在写微信小程序的时候用到了模态弹窗，但是发现微信官方给的````wx.showModal(OBJECT)````可自定义性还是太少，满足不了某些情况下的需求。

比如，用户点击了蒙层，而不是点击取消按钮，弹窗也会去执行取消事件，并且将弹窗关闭。文档里并没有屏蔽蒙层点击事件的属性，也没有办法通过其他的方式去屏蔽。

所以就写了一个可随意自定义的第三方弹窗插件【wxPopup】

使用方法也很简单，在要调用弹窗的位置使用````this.showModel()````方法，传入一些参数，就可以将弹窗显示出来。

````
this.showModel({
  ModelId: 1,
  ModelTitle: '标题(2)',
  ModelContent: '感谢使用wxPoput自定义模态弹窗,一个可以随意自定义样式的微信小程序弹窗插件。已经开源到GitHub上。'
})
````
参数| 类型 | 必填 | 说明 
|- | :-: | -: | -:
|isShowModel | Boolean | false | 控制弹窗是否显示，默认不显示
|isShowConfirm | Boolean | false | 是否只显示确定按钮，默认不是

参数| 类型 | 必填 | 说明 
|- | :-: | -: | -:
|ModelId | Number| 是 | 弹窗的id
|ModelTitle | String| 是 | 弹窗的标题 
|ModelContent | String| 是 | 弹窗文字提示内容

弹窗的取消事件
````
//取消事件
ancel: function(e){
    if (e.currentTarget.dataset.modelid == 0){
      console.log("用户点击了取消(1)")
    } else if (e.currentTarget.dataset.modelid == 1){
      console.log("用户点击了取消(2)")
    }
}
````
弹窗的确定事件
````
//确定事件
confirm: function(e){
    if (e.currentTarget.dataset.modelid == 0) {
      console.log("用户点击了确定(1)")
    } else if (e.currentTarget.dataset.modelid == 1) {
      console.log("用户点击了确定(2)")
    }
    //关闭模态弹窗
    this.setData({
      isShowModel: false
    })
}
````
可以通过判断```e.currentTarget.dataset.modelid```执行对应弹窗的事件

弹窗的显示逻辑就是这样，然后样式的话大家可以随意自定义，相信大家可以玩出更多的花样。

最后喜欢这个插件的话就给点个赞呗！
